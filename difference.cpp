#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgSeuil[250], cNomImgDilat[250];
  int nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: ImageSeuil.pgm ImageDilat.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgSeuil) ;
   sscanf (argv[2],"%s",cNomImgDilat);
   sscanf(argv[3],"%s",cNomImgEcrite);

   OCTET *ImgSeuil, *ImgOut,*ImgDilat;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgSeuil, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgDilat, &nH, &nW);

   nTaille = nH * nW;
  
   allocation_tableau(ImgSeuil, OCTET, nTaille);
   allocation_tableau(ImgDilat, OCTET, nTaille);
   lire_image_pgm(cNomImgSeuil, ImgIn, nH * nW);
   lire_image_pgm(cNomImgDilat, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);


   for (int i=1; i < nH-1; i++){ 
   for (int j=1; j < nW-1; j++)
     {

      if(ImgSeuil[i*nW+j]==255 and ImgDilat[i*nW+j]==255){
        ImgOut[i*nW+j]=255;
      }
      else if (ImgSeuil[i*nW+j]==0 and ImgDilat[i*nW+j]==0){
        ImgOut[i*nW+j]=255;
      }
      else ImgOut[i*nW+j]=0;
    }
  }

  
  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
 }

   